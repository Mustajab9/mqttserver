package com.mqtt.server.core;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@MappedSuperclass
public class BaseEntity {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "id", length = 36)
    protected String id;

    @Column(name = "created_by")
    protected String createdBy;

    @Column(name = "created_dt")
    protected LocalDateTime createdDt;

    @Column(name = "updated_by")
    protected String updatedBy;

    @Column(name = "updated_dt")
    protected LocalDateTime updatedDt;

    @Column(name = "is_deleted")
    protected Integer isDeleted;

    @Column(name = "description")
    protected String description;
}
