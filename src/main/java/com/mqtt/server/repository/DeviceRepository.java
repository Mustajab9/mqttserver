package com.mqtt.server.repository;

import com.mqtt.server.entity.Device;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DeviceRepository extends JpaRepository<Device, String> {
    List<Device> findByTenantIsActive(Integer isActive) throws Exception;
}
