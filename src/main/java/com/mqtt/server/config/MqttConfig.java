package com.mqtt.server.config;

import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.core.MessageProducer;
import org.springframework.integration.mqtt.core.DefaultMqttPahoClientFactory;
import org.springframework.integration.mqtt.core.MqttPahoClientFactory;
import org.springframework.integration.mqtt.inbound.MqttPahoMessageDrivenChannelAdapter;
import org.springframework.integration.mqtt.support.MqttHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandler;
import org.springframework.messaging.MessagingException;
import org.springframework.messaging.handler.annotation.Header;

import java.util.Map;

@Configuration
@EnableIntegration
public class MqttConfig {

//    private static final String BROKER_URL = "tcp://localhost:1883";

//    @Bean
//    public MqttConnectOptions mqttConnectOptions() {
//        MqttConnectOptions options = new MqttConnectOptions();
//        options.setServerURIs(new String[]{BROKER_URL});
//        return options;
//    }

//    @Bean
//    public MqttPahoClientFactory mqttClientFactory() {
//        DefaultMqttPahoClientFactory factory = new DefaultMqttPahoClientFactory();
//        factory.setConnectionOptions(mqttConnectOptions());
//        return factory;
//    }

//    @Bean
//    public MessageProducer inbound() {
//        MqttPahoMessageDrivenChannelAdapter adapter =
//                new MqttPahoMessageDrivenChannelAdapter("yourClientId", mqttClientFactory(), "testing-mqtt-server");
//        adapter.setCompletionTimeout(5000);
//        adapter.setQos(1);
//        return adapter;
//    }
//
//    @Bean
//    public MessageChannel mqttInputChannel() {
//        return new DirectChannel();
//    }

//    @Bean
//    public MessageProducer inbound(MqttPahoClientFactory clientFactory) {
//        MqttPahoMessageDrivenChannelAdapter adapter =
//                new MqttPahoMessageDrivenChannelAdapter("yourClientId", clientFactory, "testing-mqtt-server");
//        adapter.setOutputChannel(mqttInputChannel());
//        return adapter;
//    }

//    @Bean
//    @ServiceActivator(inputChannel = "mqttInputChannel")
//    public MessageHandler handler() {
//        return new MessageHandler() {
//            @Override
//            public void handleMessage(Message<?> message) throws MessagingException {
//                handleMqttMessage(message);
//            }
//        };
//    }

//    public void handleMqttMessage(Message<?> message) {
//        System.out.println("Received MQTT message: " + message.getPayload());
//    }

    @Bean(name = "mqttOutboundChannel")
    public MessageChannel mqttOutboundChannel() {
        return new DirectChannel();
    }

    @ServiceActivator(inputChannel = "mqttOutboundChannel")
    public void handleMqttMessage(Message<?> message) {
        System.out.println("Received MQTT message: " + message.getPayload());
    }

    @MessagingGateway
    public interface MqttMessageGateway {
        @Gateway(requestChannel = "mqttOutboundChannel")
        void sendToMqtt(Map<String, Object> data, @Header(MqttHeaders.TOPIC) String topic);
    }
}