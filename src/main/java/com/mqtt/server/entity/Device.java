package com.mqtt.server.entity;

import com.mqtt.server.core.BaseEntity;
import lombok.Data;

import javax.persistence.*;
import java.sql.Clob;

@Data
@Entity
@Table(name = "device")
public class Device extends BaseEntity {
    @Column(name = "name")
    private String name;

    @Column(name = "clob_data")
    private Clob clobData;

    @Column(name = "latitude")
    private String latitude;

    @Column(name = "longitude")
    private String longitude;

    @Column(name = "type")
    private String type;

    @ManyToOne
    @JoinColumn(name = "tenant_id", nullable = false)
    private Tenant tenant;

    @ManyToOne
    @JoinColumn(name = "project_id", nullable = false)
    private Project project;

    @Column(name = "sn")
    protected String sn;
}
