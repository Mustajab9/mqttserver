package com.mqtt.server.entity;

import com.mqtt.server.core.BaseEntity;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "user")
public class User extends BaseEntity {
    @Column(name = "company_name")
    private String companyName;

    @Column(name = "password")
    private String password;

    @Column(name = "user_name")
    private String username;

    @Column(name = "phone_no")
    private String phoneNo;
}
