package com.mqtt.server.entity;

import com.mqtt.server.core.BaseEntity;
import lombok.Data;

import javax.persistence.*;
import java.sql.Clob;

@Data
@Entity
@Table(name = "tenant")
public class Tenant extends BaseEntity {
    @Column(name = "name")
    private String name;

    @Column(name = "code")
    private String code;

    @Column(name = "latitude")
    private String latitude;

    @Column(name = "longitude")
    private String longitude;

    @ManyToOne
    @JoinColumn(name = "project_id", nullable = false)
    private Project project;

    @Column(name = "clob_data")
    private Clob clobData;

    @Column(name = "is_active")
    protected Integer isActive;
}
