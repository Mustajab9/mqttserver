package com.mqtt.server.scheduler;

import com.mqtt.server.config.MqttConfig;
import com.mqtt.server.entity.Device;
import com.mqtt.server.repository.DeviceRepository;

import lombok.Getter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.atomic.AtomicReference;

@Component
@EnableScheduling
public class GenerateResponse {
    @Autowired
    private DeviceRepository deviceRepository;

    @Autowired
    private MqttConfig.MqttMessageGateway mqttMessageGateway;

    @Getter
    private Map<String, Object> dynamicResponse = new HashMap<>();

    @Scheduled(fixedRate = 5000)
    public void getResponse() throws Exception {
        AtomicReference<String> topic = new AtomicReference<>("");
        List<Device> devices = deviceRepository.findByTenantIsActive(1);
        LocalDateTime currentDateTime = LocalDateTime.now();

        devices.forEach(device -> {
            if (device.getType().equalsIgnoreCase("ENERGYMETER")) {
                dynamicResponse.put(device.getSn() + "_voltage", generateRandom(200, 30));
                dynamicResponse.put(device.getSn() + "_current", generateRandom(200, 0));
                dynamicResponse.put(device.getSn() + "_status", 0);
                dynamicResponse.put(device.getSn() + "_date", currentDateTime);
            } else if (device.getType().equalsIgnoreCase("FLOWMETER")) {
                dynamicResponse.put(device.getSn() + "_status", 0);
                dynamicResponse.put(device.getSn() + "_flowrate", generateRandom(150, 80));
                dynamicResponse.put(device.getSn() + "_totalizer_1", 0);
                dynamicResponse.put(device.getSn() + "_totalizer_2", 0);
                dynamicResponse.put(device.getSn() + "_totalizer_3", 0);
                dynamicResponse.put(device.getSn() + "_pressure", generateRandom(200, 0));
                dynamicResponse.put(device.getSn() + "_pressure_alarm_hi", 200);
                dynamicResponse.put(device.getSn() + "_pressure_alarm_low", 0);
                dynamicResponse.put(device.getSn() + "_date", currentDateTime);
            } else {
                dynamicResponse.put(device.getSn() + "_ph", generateRandom(7, 0));
                dynamicResponse.put(device.getSn() + "_status", 0);
                dynamicResponse.put(device.getSn() + "_ph_alarm_hi", 7);
                dynamicResponse.put(device.getSn() + "_ph_alarm_low", 0);
                dynamicResponse.put(device.getSn() + "_date", currentDateTime);
            }

            topic.set(device.getTenant().getCode());
        });

        mqttMessageGateway.sendToMqtt(dynamicResponse, topic.get());
    }

    private float generateRandom(float max, float min) {
        Random random = new Random();
        return random.nextFloat() * (max - min) + min;
    }
}